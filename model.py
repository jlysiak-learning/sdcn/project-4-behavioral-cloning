'''
    Model training
    Copyrights (c) 2021 Jacek Łysiak
'''

import glob
import numpy as np
import matplotlib.pyplot  as plt
import tensorflow as tf

from dataset_generator import DatasetGenerator

class SimSequence(tf.keras.utils.Sequence):
    '''Keras wrapper for our images generator.'''
    def __init__(self, batch_size, corrections, *datasets, start=0.,  stop=1.):
        self._generator = DatasetGenerator(corrections, *datasets, start=start, stop=stop)
        self._batch_size = batch_size

    def on_epoch_end(self):
        self._generator.shuffle()

    def __len__(self):
        return len(self._generator) // self._batch_size

    def __getitem__(self, idx):
        start = self._batch_size * idx
        stop = self._batch_size * (idx + 1)
        bx, by = self._generator[start:stop]
        return bx,  by

    @property
    def shape(self):
        return self._generator.shape

datasets = glob.glob('training_datasets/*')
# Turning LEFT --> ANGLE < 0
# Correction from LEFT camera - POSITIVE, from RIGHT cam - NEGATIVE
# Steering angle is from (-1 to 1)
corrections = (0.03, -0.03)
batch_size = 16
train_seq = SimSequence(batch_size, corrections, *datasets, start=0, stop=0.9)
val_seq = SimSequence(batch_size, corrections, *datasets, start=0.9, stop=1.)

def model_custom_1(input_shape):
  inputs = tf.keras.Input(shape=input_shape)
  x = tf.keras.applications.resnet_v2.preprocess_input(inputs)
  x = tf.keras.layers.Conv2D(36, 5, activation='relu')(x)
  x = tf.keras.layers.MaxPool2D()(x)
  x = tf.keras.layers.Dropout(0.2)(x)
  x = tf.keras.layers.Conv2D(48, 5, activation='relu')(x)
  x = tf.keras.layers.MaxPool2D()(x)
  x = tf.keras.layers.Dropout(0.2)(x)
  x = tf.keras.layers.Conv2D(64, 5, activation='relu')(x)
  x = tf.keras.layers.MaxPool2D()(x)
  x = tf.keras.layers.Dropout(0.4)(x)
  x = tf.keras.layers.Conv2D(86, 3, activation='relu')(x)
  x = tf.keras.layers.MaxPool2D()(x)
  x = tf.keras.layers.Dropout(0.4)(x)
  x = tf.keras.layers.Conv2D(100, 2, activation='relu')(x)
  x = tf.keras.layers.Dropout(0.4)(x)
  x = tf.keras.layers.Flatten()(x)
  x = tf.keras.layers.Dense(200, activation='relu')(x)
  x = tf.keras.layers.Dropout(0.4)(x)
  x = tf.keras.layers.Dense(100, activation='relu')(x)
  x = tf.keras.layers.Dropout(0.4)(x)
  x = tf.keras.layers.Dense(20, activation='relu')(x)
  x = tf.keras.layers.Dropout(0.2)(x)
  outputs = tf.keras.layers.Dense(1)(x)
  model = tf.keras.Model(inputs, outputs)
  model.summary()
  return model

custom_1 = model_custom_1(train_seq.shape)
base_learning_rate = 0.001
custom_1.compile(
    optimizer=tf.keras.optimizers.Adam(lr=base_learning_rate),
    loss=tf.keras.losses.MeanSquaredError())

history = custom_1.fit(train_seq, epochs=20, validation_data=val_seq)
custom_1.save('model.h5')

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model mean squared error loss')
plt.ylabel('mean squared error loss')
plt.xlabel('epoch')
plt.legend(['training set', 'validation set'], loc='upper right')
plt.show()
