# **Behavioral Cloning** - a poor's man attempt to control a car in a sim with a neural network

## Gathering the data

A base dataset has been collected from rides on two maps (basic and advances)
in two directions (CW and CCW) to augment the dataset.
During the base runs I've tried to keep the car in the middle of the track and
used mouse to control the car smoothly as using keyboard generated a lot of
step-like steering angle data.

In the further stages of the project, it turned out that a base dataset is not
sufficient, therefore another runs has been recorded. These runs included
situations where the car was driving close to edges and steering angle was set
in the opposite direction. It helped with learning the model how to rescue when
the car is try to cross edge of the track.

## Preparing training dataset

Special hepler script has been written to preview recorded data, crop raw frames
and select appropriate frames. Instructions how to use it are included in
[README](./README.md).

The following parameters has been used to prepare datasets:
- `-s 1`: no low pass filter, as the data from mouse was smooth enough
- `-c 0.35 0 0.9 1`: raw images were cropped only on y-axis, from 35% to 90% of
  the original images

During the process I've rejected all frames where I drove a car outside or I was
driving it outwards without turning.

Dataset image example is shown below:

![img](./examples/ds_center.jpg)

Image shape is `320x88x3`.

## Model

I've tried a couple of models. Each time I've trained a model, I downloaded it
from Google Colab and run `drive.py` to check it's behaviour in simulator.

I started with using pretrained Resnet or Mobilenetv3 large model, and adding
a few dense layer on a top of them, but their inference time wasn't too good
on my hardware, as the models were to heavy.

Finally, I've decided to train a simple and small model from the scratch.

Architecture of my model is as follows:
```
Layer (type)                 Output Shape              Param #   
=================================================================
input_1 (InputLayer)         [(None, 88, 320, 3)]      0         
_________________________________________________________________
tf.math.truediv (TFOpLambda) (None, 88, 320, 3)        0         
_________________________________________________________________
tf.math.subtract (TFOpLambda (None, 88, 320, 3)        0         
_________________________________________________________________
conv2d (Conv2D)              (None, 84, 316, 36)       2736      
_________________________________________________________________
max_pooling2d (MaxPooling2D) (None, 42, 158, 36)       0         
_________________________________________________________________
dropout (Dropout)            (None, 42, 158, 36)       0         
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 38, 154, 48)       43248     
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 19, 77, 48)        0         
_________________________________________________________________
dropout_1 (Dropout)          (None, 19, 77, 48)        0         
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 15, 73, 64)        76864     
_________________________________________________________________
max_pooling2d_2 (MaxPooling2 (None, 7, 36, 64)         0         
_________________________________________________________________
dropout_2 (Dropout)          (None, 7, 36, 64)         0         
_________________________________________________________________
conv2d_3 (Conv2D)            (None, 5, 34, 86)         49622     
_________________________________________________________________
max_pooling2d_3 (MaxPooling2 (None, 2, 17, 86)         0         
_________________________________________________________________
dropout_3 (Dropout)          (None, 2, 17, 86)         0         
_________________________________________________________________
conv2d_4 (Conv2D)            (None, 1, 16, 100)        34500     
_________________________________________________________________
dropout_4 (Dropout)          (None, 1, 16, 100)        0         
_________________________________________________________________
flatten (Flatten)            (None, 1600)              0         
_________________________________________________________________
dense (Dense)                (None, 200)               320200    
_________________________________________________________________
dropout_5 (Dropout)          (None, 200)               0         
_________________________________________________________________
dense_1 (Dense)              (None, 100)               20100     
_________________________________________________________________
dropout_6 (Dropout)          (None, 100)               0         
_________________________________________________________________
dense_2 (Dense)              (None, 20)                2020      
_________________________________________________________________
dropout_7 (Dropout)          (None, 20)                0         
_________________________________________________________________
dense_3 (Dense)              (None, 1)                 21        
=================================================================
Total params: 549,311
Trainable params: 549,311
Non-trainable params: 0
```
The model is created in `model.py`, lines: `45 - 70`.

For training I've used ADAM optimizer with `batch_size=64` and
`base_learning_rate=0.001`. The model has been trained 20 epochs.

As the output of the model is just a number, a steering value from `-1` to `1`,
mean squared error has been used as a loss function.

90% of dataset has been used as training data (line 41) and 20% as validation
dataset (line 42).

Collected dataset has been also augmented using vertical flip (it is implemented
in `dataset_generator.py:60 in _load_info function`).
I is important to negate a steering value when image is mirrored.

## Simulator changes

In `drive.py` I've done the following changes:

- `line 55`: max speed set to `18`
- `line 56`: min speed set to `5`
- `lines 65-72, 90`: raw image cropping
- `lines 99-100`: car speed depends on how much the car is turning (it slows
  down), it helps with stabilizing the car when it tries to turn too fast.
- 'line 60`: model output is multiplied by a factor to tune it performance in
  the sim
