# sdcn-4-behavioral-cloning
Cloning human player behavior to control a car with neural net in a sim.

Starter code comes from: https://github.com/udacity/CarND-Behavioral-Cloning-P3

Simulator can be found here: https://github.com/udacity/self-driving-car-sim

## Files
- `dataset_generator.py`: Python generator loading dataset from directories
- `drive.py`: server that controls a car in the sim
- `get_datasets.sh`: downloads dataset to train a model
- `model.h5`: train model
- `model.py`: script to train a model
- `Pipfile`: env description for `pipenv`
- `prepare_dataset.py`: raw sim images to training dataset converter
- `sdcn_4_transfer.ipynb`: final notebook used to train a model on Google Colab
- `video.mp4`: video of autonomous run
- `video.py`: frames-to-video converter
- `writeup_report.md`: few words about solution


## Training the model

There are two options:

1. upload notebook into Colab

2. download dataset with `get_datasets.sh` and try to run `model.py`, but
    remember about installing deps

## Running the model

1. Download the sim (there are links to download TERM1 sim as zip file)
2. Run sim
3. Run `pipenv run python drive.py model.h5` (if using `pipenv` ofc, if not
    you need to figure it out)
4. Select autonomous mode in the sim
5. Watch your model driving a car.

## Preparing a training dataset

1. Record your run in the sim. When you drive, hit `R` and select directory to
   save frames to.
2. Once your rides are recorded, i.e. in `rides/0`, `rides/1`, etc. rides are
   recorded, i.e. in `rides/0`, `rides/1`, etc. we can prepare datasets.
   Run `./prepare_dataset.py <DS> -s <F> -c <y1 x1 y2 x2> <OUT>`, where:
    - `<DS>`: raw frames directory, i.e. `rides/0`
    - `<F>`: filtering factor (float in range 0 - 1), low pass filter param to
        smooth steering angle recorded during the run. It's useful if we control a
        car using keyboard only.
    - `<y1 x1 y2 x2>`: cropping factors, 4 floats in range 0-1, used to crop
      original images. Reccomended values: `0.35 0 0.9 1`. Then, the most
      important part of the road is shown in the pics.
    - `<OUT>`: output directory for selected frames
3. Now you should see a window with a frame preview, a line showing approximate
   steering angle and it's value. Press `y` to add this frame to your dataset.
   Press `Esc` or `q` to abort, and any other key to skip this frame.

## Using the dataset

Once your rides are converted and stored i.e. in `datasets/*` directories you
can use them to instantiate `DatasetGenerator` object. It dynamically opens and
reads images from the drive during a training without storing a whole dataset in
the memory.

Important `DatasetGenerator` arguments:
- `corrections`: tuple `(left correctioin, right correction)`, steering angle
  corrections for left and right camera images. As steering signal is in range
  `(-1, 1)`, these values should be in range `(0, 1)` and `left correction > 0`
  and `right correction < 0`.
- `*dataset_paths`: a list of dataset paths, i.e. `glob('datasets/*')`
- `start`, `stop`: floats in range `(0, 1)`, selects a part of each of provided
  datasets that is used by a generator. Useful when dividing datasets into
  training and validation datasets.

## Solution

For more information about solution, please, refer to [writeup](./writeup_report.md).
