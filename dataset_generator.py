'''
    Dynamic dataset generatioin.
    Copyrights (c) 2021 Jacek Łysiak
'''

import os
import random
import cv2
import numpy as np

class DatasetGenerator:
    def __init__(self, corrections, *dataset_paths, no_shuffle=False,
            limit_examples=0, print_log=False, start=0., stop=1.):
        assert type(corrections) is tuple
        assert len(corrections) == 2
        self._corrections = corrections
        assert len(dataset_paths) > 0
        self._dataset_paths = list(dataset_paths)
        self._no_shuffle = no_shuffle
        self._limit_examples = limit_examples
        self._print_log = print_log
        self._start = start
        self._stop = stop
        self._shape = self._extract_shape()
        self._load_info()
    
    def _extract_shape(self):
        cs, _, _, _ = self._load_dataset_info(self._dataset_paths[0])
        path = cs[0]
        img = cv2.imread(path)
        return tuple(img.shape)

    def _load_dataset_info(self, path):
        angles_path = os.path.join(path, 'angle.csv')
        cs_path = os.path.join(path, 'center')
        ls_path = os.path.join(path, 'left')
        rs_path = os.path.join(path, 'right')
        with open(angles_path) as f:
            angles = list(map(float, f.read().split()))
        cs = [os.path.join(cs_path, x) for x in os.listdir(cs_path)]
        ls = [os.path.join(ls_path, x) for x in os.listdir(ls_path)]
        rs = [os.path.join(rs_path, x) for x in os.listdir(rs_path)]
        cs.sort()
        ls.sort()
        rs.sort()
        n = len(cs)
        idx_start = int(n * self._start)
        idx_stop = int(n * self._stop)
        cs = cs[idx_start:idx_stop]
        ls = ls[idx_start:idx_stop]
        rs = rs[idx_start:idx_stop]
        angles = angles[idx_start:idx_stop]
        if self._limit_examples > 0:
            cs = cs[:self._limit_examples]
            ls = ls[:self._limit_examples]
            rs = rs[:self._limit_examples]
            angles = angles[:self._limit_examples]
        return cs, ls, rs, angles
    
    def _load_info(self):
        # get all (img, angle)
        examples = []
        for dataset_path in self._dataset_paths:
            cs, ls, rs, angles = self._load_dataset_info(dataset_path)
            # Add non-flipped imgs
            flipped = [False] * len(angles)
            examples.extend(zip(cs, angles, flipped))
            examples.extend(zip(ls, [a + self._corrections[0] for a in angles], flipped))
            examples.extend(zip(rs, [a + self._corrections[1] for a in angles], flipped))
            # Add flipped
            flipped = [True] * len(angles)
            examples.extend(zip(cs, [-a for a in angles], flipped))
            examples.extend(zip(ls, [-a - self._corrections[0] for a in angles], flipped))
            examples.extend(zip(rs, [-a - self._corrections[1] for a in angles], flipped))
        self._examples = examples
        self._size = len(examples)
        
    def shuffle(self):
        random.shuffle(self._examples)

    def __iter__(self):
        if not self._no_shuffle:
            self.shuffle()
        self._idx = 0
        return self

    def __len__(self):
        return self._size
    
    def _load_example(self, idx):
        path, angle, flip = self._examples[idx]
        if self._print_log:
            print('Next up: ', path, angle, flip)
        img = cv2.imread(path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        if flip:
            img = img[:,::-1,:]
        return img, angle
        
    def __next__(self):
        # Get next image from dataset
        if self._idx < self._size:
            img, angle = self._load_example(self._idx)
            self._idx += 1
            return img, angle
        else:
            raise StopIteration
            
    def __getitem__(self, args):
        if type(args) is slice:
                imgs =  []
                angles = []
                for idx in range(args.start, args.stop):
                    img, a = self._load_example(idx)
                    imgs.append(img)
                    angles.append(a)
                return np.array(imgs), np.array(angles)
        else:
            idx = args[0]
            return self._load_example(idx)

    @property
    def shape(self):
        return self._shape
        

if __name__ == '__main__':
    import sys
    gen = DatasetGenerator((5, -5), *tuple(sys.argv[1:]), no_shuffle=True,
        limit_examples=5, print_log=True)
    for img, angle in gen:
        cv2.imshow('prev', img)
        key = cv2.waitKey(0)
        if key == 27:
            break
