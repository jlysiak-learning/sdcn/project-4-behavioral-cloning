#!/usr/bin/env python3
'''
    Dataset preparation from raw images from the sim.
    Copyrights (c) 2021 Jacek Łysiak
'''


import pandas as pd
import numpy as np
import cv2
import os
from argparse import ArgumentParser


def put_text(img, text):
    font = cv2.FONT_HERSHEY_SIMPLEX
    org = (00, 30)
    fontScale = 0.6
    color = (0, 4, 255)
    thickness = 1
    cv2.putText(img, text, org, font, fontScale, color, thickness, cv2.LINE_AA)

class SimDataset:

    # Original dataset structure
    IMG_DIR = 'IMG'
    CSV_FILE = 'driving_log.csv'

    # My structure
    LEFT_DIR = 'left'
    RIGHT_DIR = 'right'
    CENTER_DIR = 'center'
    ANGLE_FILE = 'angle.csv'

    def __init__(self, sim_dataset_dir, smoothness=1., crop=(0, 0, 1, 1)):
        self._root_dir = sim_dataset_dir
        self._log_file = os.path.join(self._root_dir, self.CSV_FILE)
        self._img_dir = os.path.join(self._root_dir, self.IMG_DIR)
        self._alpha = smoothness
        self._crop_params = crop
        self._selected_data = None

    def select_frames(self):
        raw_it = self._raw_dataset_iterator()
        accepted = []
        for name_c, name_l, name_r, angle, raw_angle in raw_it():
            path_c = os.path.join(self._img_dir, name_c)
            if not self._preview(path_c, angle):
                continue
            print('Accept image: %s, angle=%.03f raw_angle=%.03f' % (name_c,
                angle, raw_angle))
            accepted.append((name_c, name_l, name_r, angle, raw_angle))

        self._selected_angles = [angle for _, _, _, angle, _ in accepted]
        self._selected_raw_angles = [angle for _, _, _, _, angle in accepted]
        self._selected_frames = [(c, l, r) for c, l, r, _, _ in accepted]
        self._selected_data = accepted
        print('Frames selection finished!')

    def save(self, output_dir):
        assert self._selected_data
        os.makedirs(output_dir)
        lp = os.path.join(output_dir, self.LEFT_DIR)
        rp = os.path.join(output_dir, self.RIGHT_DIR)
        cp = os.path.join(output_dir, self.CENTER_DIR)
        os.makedirs(lp)
        os.makedirs(rp)
        os.makedirs(cp)
        angle_file = os.path.join(output_dir, self.ANGLE_FILE)

        def _save(name, outdir, idx):
            src_path = os.path.join(self._img_dir, name)
            dst_path = os.path.join(outdir, '%05d.jpg' % idx)
            self._save_image(src_path, dst_path)

        with open(angle_file, 'w') as f:
            for idx, (c, l, r, a, _) in enumerate(self._selected_data):
                _save(c, cp, idx)
                _save(l, lp, idx)
                _save(r, rp, idx)
                f.write('%f\n' % a)
        print('Saved!')

    def _save_image(self, src, dst):
        img = cv2.imread(src)
        img = self._crop(img)
        cv2.imwrite(dst, img)

    def _plot_selected_angles(self):
        # Python crashes if matplotlib is used with opencv
        #import matplotlib.pyplot as plt
        #plt.plot(angles)
        #plt.plot(raw_angles)
        #plt.show()
        pass

    def _crop(self, img):
        ys, xs, _ = tuple(img.shape)
        x1 = int(xs * self._crop_params[1])
        x2 = int(xs * self._crop_params[3])
        y1 = int(ys * self._crop_params[0])
        y2 = int(ys * self._crop_params[2])
        return img[y1:y2, x1:x2, :]

    def _preview(self, img_path, angle):
        img = cv2.imread(img_path)
        img = self._crop(img)
        pt1 = (img.shape[1] // 2, img.shape[0])
        angle_deg = angle * 25
        angle_rad = angle_deg / 180 * np.pi
        pt2 = (int(img.shape[1] / 2 + 60 * np.sin(angle_rad)),
                int(img.shape[0] - 60 * np.cos(angle_rad)))
        img = cv2.line(img, pt1, pt2, (255, 0, 0), 3)
        put_text(img, 'angle: %f' % angle_deg)
        cv2.imshow('preview', img)
        key = cv2.waitKey(0)
        if key == ord('q') or key == 27: # ESC, quit
            print('Aborting...')
            exit(0)
        elif key == ord('y'):
            return True
        return False

    def _raw_dataset_iterator(self):
        def _generator():
            angle = 0.
            with open(self._log_file) as f:
                for line in f:
                    path_c, path_l, path_r, steer, _, _, _ = self._parse_line(line)
                    name_c, name_l, name_r = self._img_names(path_c, path_l, path_r)
                    steer = float(steer)
                    angle = angle * (1 - self._alpha) + steer * self._alpha
                    yield name_c, name_l, name_r, angle, steer
        return _generator

    def _img_names(self, *abs_paths):
        return tuple([os.path.basename(p) for p in abs_paths])

    def _parse_line(self, line):
        return tuple(line.split(','))

def parse_args():
    parser = ArgumentParser(
        description='Path to directory with data from the simulator')
    parser.add_argument(
        '-s', '--smoothness', type=float, default=1.,
        help='Smoothing steering angle factor, (0-1)')
    parser.add_argument(
        '-c', '--crop', nargs=4, type=float,
        metavar=('top', 'left', 'bottom', 'right'), default=(0, 0, 1, 1),
        help='Cropping params: 4 floats in range (0-10')
    parser.add_argument('sim_dataset')
    parser.add_argument('output_dir')
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    if os.path.isdir(args.output_dir):
        print("Path %s exists" % args.output_dir)
        exit(1)

    sim_dataset = SimDataset(
        args.sim_dataset, smoothness=args.smoothness, crop=args.crop)
    sim_dataset.select_frames()
    sim_dataset.save(args.output_dir)

